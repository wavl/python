from wavl import WAVLTree

import hypothesis
import hypothesis.strategies as st
import logging
import pytest

logger = logging.getLogger(__name__)


def test_empty():
    tree = WAVLTree()

    assert tree.root is None
    assert tree.is_correct


def test_one_node():
    tree = WAVLTree()
    tree.insert(1)

    assert tree.root is not None
    assert 1 == tree.root.value
    assert 0 == tree.root.rank
    assert tree.root.left is None
    assert tree.root.right is None

    assert tree.is_correct


@pytest.mark.parametrize("values", [[1, 2], [1, 2, 0]])
def test_no_rebalance_needed(values):
    tree = WAVLTree()

    for value in values:
        tree.insert(value)
        assert tree.is_correct


def test_three_nodes_rebalanced():
    tree = WAVLTree()

    for value in (1, 2, 3):
        print(tree)
        tree.insert(value)

    assert tree.is_correct


def test_bigger_tree():
    tree = WAVLTree()

    for i in range(50):
        tree.insert(i)
        assert tree.is_correct


def test_bigger_tree_reversed():
    tree = WAVLTree()

    for i in range(50):
        tree.insert(-i)
        assert tree.is_correct


def test_promote():
    tree = WAVLTree()

    for value in (0, 1, -1):
        tree.insert(value)
        assert tree.is_correct


@pytest.mark.parametrize(
    "values",
    [
        [0, 1, -2, -1, -3],
        [0, 1, -2, -1, -3, -4, 4, 9, 7],
        [0, 1, -2, -1, -3, -4, 4, 9, 7, 5, -5, 8],
    ],
)
def test_rotate(values):
    tree = WAVLTree()

    for value in values:
        tree.insert(value)
        assert tree.is_correct


@hypothesis.given(values=st.sets(st.integers()))
def test_search_random(values):
    tree = WAVLTree()

    for value in values:
        tree.insert(value)
        assert tree.is_correct

    for value in values:
        assert tree.search(value) is not None


# @unittest.skip("Only for replicating hypothesis")
@pytest.mark.parametrize(
    "values, delete_order",
    [
        ([0, 1, 2], [0, 2, 1]),
        ([0, 1, 2, -1], [2, 0, 1, -1]),
        ([0, 1, -1], [0, -1, 1]),
        ([0, 1], [0, 1]),
        ([0, 1, 32, -1], [32, 0, 1, -1]),
    ],
)
def test_delete_minimal(values, delete_order):
    tree = WAVLTree()

    for value in values:
        tree.insert(value)

    for value in delete_order:
        before = str(tree)
        tree.delete(value)
        after = str(tree)

        try:
            assert tree.is_correct
        except AssertionError:
            logger.info(
                f"[FAIL] Delete {value} from {values} in order {delete_order}"
            )
            logger.info(f"Before:\n{before}")
            logger.info(f"After:\n{after}")
            raise
