from avl import AVLTree
from wavl import WAVLTree
from comparator import Comparator
from test_properties import delete_strategy

import hypothesis
import pytest


def report_different(before, deleted, comparator):
    h = abs(hash(before))
    with (
        open(f"trees/{h}_before.dot", "w") as b,
        open(f"trees/{h}_d{deleted}_after.dot", "w") as a,
    ):
        print(before, file=b)
        print(comparator, file=a)


@pytest.mark.xfail
@hypothesis.settings(max_examples=10000, deadline=None)
@hypothesis.given(config=delete_strategy())
def test_delete_ranks_differ(config):
    values, order = config

    comparator = Comparator(AVLTree(), WAVLTree())

    for value in values:
        comparator.insert(value)

    for value in order:
        before = str(comparator.left)
        comparator.delete(value)

        try:
            assert comparator.are_equal
        except AssertionError:
            report_different(before, value, comparator)
            raise


@pytest.mark.xfail
@hypothesis.settings(max_examples=10000, deadline=None)
@hypothesis.given(config=delete_strategy())
def test_delete_trees_differ(config):
    values, order = config

    comparator = Comparator(AVLTree(), WAVLTree())

    for value in values:
        comparator.insert(value)

    for value in order:
        before = str(comparator.left)
        comparator.delete(value)

        try:
            assert comparator.are_similar
        except AssertionError:
            report_different(before, value, comparator)
            raise
