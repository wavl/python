from node import Node


def test_default_rank():
    node = Node(0)

    assert 0 == node.value
    assert node.parent is None
    assert node.left is None
    assert node.right is None

    assert 0 == node.rank
    assert 0 == Node.get_rank(node)
    assert -1 == Node.difference(node)
    assert (1, 1) == Node.differences(node)
