from rbt import RBTree

import logging
import pytest

logger = logging.getLogger(__name__)


@pytest.mark.parametrize(
    "values, delete_order",
    [
        ([0, 1, -1], [0, -1, 1]),
        ([0, 1, 2, 3, 4, 5], [4, 2, 1, 0, 5, 3]),
        (
            [32, 1, 2, 3, 4, 5, 0, -2, -4, -3, -1],
            [-4, -3, 1, 2, 5, 3, -2, 4, 32, -1, 0],
        ),
        ([0, 1, 2, -2, -3, -1], [-3, 2, 1, 0, -1, -2]),
        ([0, 1, 2, 3, 4, 5, -1], [4, 2, 1, 0, 5, 3, -1]),
    ],
)
def test_delete_minimal(values, delete_order):
    tree = RBTree()

    for value in values:
        tree.insert(value)

    for value in delete_order:
        logger.info("Deleting %s", value)

        before = str(tree)
        tree.delete(value)
        after = str(tree)

        try:
            assert tree.is_correct
        except AssertionError:
            logger.info(
                f"[FAIL] Delete {value} from {values} in order {delete_order}"
            )
            logger.info(f"Before:\n{before}")
            logger.info(f"After:\n{after}")
            raise
