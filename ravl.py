from wavl import WAVLTree
from node import Node, Comparable

import logging
from typing import TypeVar, Optional

logger = logging.getLogger(__name__)
T = TypeVar("T", bound=Comparable)


class RAVLTree(WAVLTree[T]):
    def is_correct_node(
        self, node: Optional[Node[T]], recursive: bool = True
    ) -> bool:
        if not node:
            return True

        if not all(child_rank > 0 for child_rank in Node.differences(node)):
            return False

        return not recursive or (
            self.is_correct_node(node.left)
            and self.is_correct_node(node.right)
        )

    # region DeleteRebalance

    def _delete_rebalance(
        self, node: Optional[Node[T]], parent: Optional[Node[T]]
    ) -> None:
        # There is no rebalance after delete in rAVL tree
        pass

    # endregion DeleteRebalance
