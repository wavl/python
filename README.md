# WAVL Tree Implementation

## Used dependencies

Dependencies are used mainly in tests, namely:

- `hypothesis`
- `pytest`

## Running tests

When finding a counter-example, hypothesis may generate **a lot** of DOT files
with generated trees.