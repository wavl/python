from comparator import Comparator
from avl import AVLTree
from wavl import WAVLTree

import logging
import random


def main():
    logging.basicConfig(level=logging.DEBUG)

    t = Comparator(AVLTree(), WAVLTree())
    # t = WAVLTree()

    values = []
    for _ in range(42):
        x = random.randint(1, 100)
        while x in values:
            x = random.randint(1, 100)

        values.append(x)
        t.insert(x)

    for _ in range(5):
        x = random.choice(values)
        values.remove(x)
        t.delete(x)

    for _ in range(42):
        x = random.randint(1, 100)
        while x in values:
            x = random.randint(1, 100)

        values.append(x)
        t.insert(x)

    for _ in range(5):
        x = random.choice(values)
        values.remove(x)
        t.delete(x)

    print(t)
    # print(t.are_equal)


if __name__ == "__main__":
    main()
