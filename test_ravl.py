from ravl import RAVLTree

import logging
import pytest

logger = logging.getLogger(__name__)


def test_empty():
    tree = RAVLTree()

    assert tree.root is None
    assert tree.is_correct


def test_one_node():
    tree = RAVLTree()
    tree.insert(1)

    assert tree.root is not None
    assert 1 == tree.root.value
    assert 0 == tree.root.rank
    assert tree.root.left is None
    assert tree.root.right is None

    assert tree.is_correct


@pytest.mark.parametrize("values", [[1, 2], [1, 2, 0]])
def test_no_rebalance_needed(values):
    tree = RAVLTree()

    for value in values:
        tree.insert(value)
        assert tree.is_correct


def test_three_nodes_rebalanced():
    tree = RAVLTree()

    for value in (1, 2, 3):
        print(tree)
        tree.insert(value)

    assert tree.is_correct


def test_bigger_tree():
    tree = RAVLTree()

    for i in range(50):
        tree.insert(i)
        assert tree.is_correct


def test_bigger_tree_reversed():
    tree = RAVLTree()

    for i in range(50):
        tree.insert(-i)
        assert tree.is_correct


def test_promote():
    tree = RAVLTree()

    for value in (0, 1, -1):
        tree.insert(value)
        assert tree.is_correct
