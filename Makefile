check:
	pytest -vvv --log-level debug --cov --cov-report=term-missing --cov-report=xml:coverage.xml --junitxml=report.xml

lint: black flake8 mypy

black:
	black --line-length=79 *.py

flake8:
	flake8 *.py

mypy:
	mypy --strict --disallow-any-explicit node.py ranked_tree.py avl.py wavl.py ravl.py rbt.py

clean:
	rm -rf __pycache__ .hypothesis .mypy_cache .pytest_cache
